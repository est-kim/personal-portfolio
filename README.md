Hello!
### Technologies
<!-- The project was built with [Next.js](https://nextjs.org), [Chakra UI](https://chakra-ui.com/), and deployed on [Vercel](https://vercel.com). -->

### Build
```
$ git clone https://github.com/est-kim/personal-portfolio.git
$ cd personal-portforlio
$ npm install
$ npm run dev
# or yarn
```
Open up http://localhost:3000 to see the result.
